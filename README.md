# mst-semester

[[_TOC_]]

## Installation

Command Line:

```bash
pip install mst-semester --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-semester
```

## Usage

mst-semester contains methods for working with school terms.

```python
from mst.semester import Semester

sem = Semester()

current_term = sem.get_school_term(term_format='object')
next_term = current_term.next_term()
prior_term = current_term.previous_term()
if current_term and next_term and prior_term:
    print(f"{prior_term.term} -> {current_term.term} -> {next_term.term}")

```
